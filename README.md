# Jpeg Deduper





## Overview

**Jpeg Deduper**, for people who have duplicate ".jpg" images filling up their harddrive.

### Usage

##### python undupeJpgs.py [-h] [-f DUPLICATEDIR] [-d] [-v] directory

A single directory is the only required argument.

Example:

```
python undupeJpgs.py /Users/batman/pictures
```

In the example above, the "/Users/batman/pictures" directory will be searched and if any duplicate images are found, they will be placed in a new, randomly named directory.

### Positional arguments:

##### directory
  
Specify a directory that contains duplicate JPEG files.  This is the only required argument.

### Optional arguments:

##### -h, --help

show the help message and exit

##### -f DUPLICATEDIR 
or
##### --duplicateDir DUPLICATEDIR

Using the -f or --duplicateDir options, specify a directory to be used/created to store duplicate images.

Example:

```
python jpegDeduper.py -f "/tmp" "/Users/batman/pictures"
```

##### -d
or
##### --delete-duplicates

Automatically delete duplicate images

##### -v
or 
##### --verbose

Increase output verbosity.