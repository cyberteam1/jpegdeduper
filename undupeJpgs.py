#!/usr/bin/env python
"""Separate all duplicate images in a directory and it's subdirectories!
"""
__author__ = "Marcus Dandrea"
__copyright__ = "Copyright 2015, Cyber Team 1"
__credits__ = ["Marcus Dandrea", "Superman", "Spiderman", "Batman"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Batman"
__email__ = "dandrea.marcus@gmail.com"
__status__ = "Testing"

import os
import argparse 
import hashlib
import shutil
import random
import string
import sys
import pdb

def find_jpgs(directory): # Function that takes a dirctory, finds all jpg files and creates a list including the full path to each file
	jpegs = []
	for dirname, dirs, files in os.walk(directory):
		for filename in files:
			ext = os.path.splitext(filename)[-1].lower()
			if ext == ".jpg":
				pathToFile = os.path.join(dirname, filename)
				jpegs.append(pathToFile)
	jpegs.sort()
	return jpegs

def iso_orig(list_images): # Takes a list of jpg files, gathers information on each file
	isolated_jpgs = {}
	duplicates = []
	for jpg in list_images:
		readjpg = open(jpg, "rb").read()
		hashjpg = hashlib.md5(readjpg).hexdigest()
		if hashjpg not in isolated_jpgs.values():
			isolated_jpgs[jpg] = hashjpg
		else:
			duplicates.append(jpg)
	return (isolated_jpgs, duplicates)

def random_str(strlen):
	new_string = ''.join(random.SystemRandom().choice(string.uppercase + string.digits) for _ in xrange(strlen))
	return new_string

def make_dir(new_dir):
	if not os.path.exists(new_dir):
		os.makedirs(new_dir)
	return os.path.abspath(new_dir)

def move_list(list_dupes, dupe_dir):
	for each in list_dupes: 
		shutil.move(each, dupe_dir)

def rename_list_files(list_images):
	new_list = []
	for each in list_images:
		new_str = random_str(12)
		new_name = new_str + '.jpg'
		old_path = os.path.split(each)
		new_path = "/".join([old_path[0], new_name])
		os.rename(each, new_path)
		new_list.append(new_path)
	return new_list

def delete_list_files(list_images):
	for each in list_images:
		os.unlist(each)


parser = argparse.ArgumentParser(description='Find all JPEG images in a directory and delete duplicates')
parser.add_argument('directory', help='Specify a directory that contains duplicate JPEG files')
parser.add_argument('-f', '--duplicateDir', help='Specify a directory to be created and/or used for duplicate images')
parser.add_argument('-d', '--delete-duplicates', action='store_true', help='Automatically delete duplicates')
parser.add_argument('-v', '--verbose', action='store_true', help='Increase output verbosity')
args = parser.parse_args()


if __name__ == "__main__":

	if args.delete_duplicates:
		delete = raw_input("Are you sure you want to delete duplicate images? (y/n) ").lower()
		while delete not in ['y', 'n']:
			sys.exit()
		if delete == 'n':
			print("Thank you. Exiting.")
			sys.exit()

	w = args.verbose

	if w:
		print("Finding jpg images...")
	x = find_jpgs(args.directory)
	if w:
		print(x)

	if w:
		print("Separating duplicate images...")
	y = iso_orig(x)
	if w:
		print("Good files to keep: ")
		print(y[0])
		print("Duplicates: ")
		print(y[1])

	if w:
		print("Preparing duplicates for temporary storage...")
	z = rename_list_files(y[1])
	if w:
		print("Temporary file paths: ")
		print(z)

	if args.duplicateDir:
		make_dir(args.duplicateDir)
		if w:
			print("Moving duplicates to specified directory..." + args.duplicateDir)
		move_list(z, args.duplicateDir)
		print("Duplicates moved to " + args.duplicateDir)
	else:
		if w:
			print("Producing random string for temporary directory...")
		a = random_str(12)
		if w:
			print a

		if w:
			print("Creating directory...")
		b = make_dir(a)
		if w:
			print(b)
		if w:
			print("Moving files to temporary directory...")
		move_list(z, b)
		if w:
			c = os.listdir(b)
			print("Directory contents: ")
			for d in c:
				print(d)
		if args.delete_duplicates:
			if w:
				print("Creating list of duplicates to delete...")
			e = find_jpgs(b)
			print("Files to be deleted: ")
			for f in e:
				print(f)
			delete_list_files(e)
			print("Duplicates successfully deleted.")
